<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/trix.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


    <style>
        .level {
            display: flex;
            align-items: center;
        }
        .level-item { margin-right: 1em;}
        .flex {
            flex: 1;
        }
        [v-cloak] { display: none;}
    </style>
    @yield('styles')
</head>
<body>
<div id="app">

    @include('layouts.nav')

    <main class="py-4">
        @yield('content')
    </main>

    <flash message="{{ session('flash') }}"></flash>

</div>
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
    window.App = {!! json_encode([
        'csrf_token' => csrf_token(),
        'user' => auth()->user(),
        'signedIn' => auth()->check()
    ]) !!};
</script>
</body>
</html>
