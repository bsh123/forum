<?php

namespace App\Listeners;


use App\Events\ThreadReceivedNewReply;
use App\Notifications\YouWereMentioned;
use App\User;

class NotifyMentionedUsers
{
    /**
     * Handle the event.
     *
     * @param ThreadReceivedNewReply $event
     * @return void
     */
    public function handle(ThreadReceivedNewReply $event)
    {
        // Way 1
        /*$mentionedUsers = $event->reply->mentionedUsers();
        // And for each mentioned user, notify them.
        foreach ($mentionedUsers as $name) {
            if ($user = User::whereName($name)->first()) {
                $user->notify(new YouWereMentioned($this));
            }
        }*/

        // Way 2
        User::whereIn('name', $event->reply->mentionedUsers())
            ->get()
            ->each(function ($user) use ($event) {
                $user->notify(new YouWereMentioned($event->reply));
            });
    }
}
