{{--Editing thread--}}
<div class="card" v-if="editing">
    <div class="card-header">

        <div class="level">
            <input type="text" class="form-control" v-model="form.title">
        </div>

    </div>

    <div class="card-body">
        <wysiwyg name="body" v-model="form.body"></wysiwyg>
    </div>

    <div class="card-footer">
        <div class="level">
            <button class="btn btn-primary btn-sm level-item" @click="update">Update</button>
            <button class="btn btn-secondary btn-sm level-item" @click="resetForm">Cancel</button>
            @can('update', $thread)
                <form method="POST" action="{{ $thread->path() }}" class="ml-auto">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm">Delete Thread</button>
                </form>
            @endcan
        </div>
    </div>
</div>

{{--Viewing thread--}}
<div class="card" v-else>
    <div class="card-header">

        <div class="level">
            <img src="{{ $thread->creator->avatar_path }}" alt="avatar" width="40" class="mr-1 rounded-circle">

            <span class="flex">
                <a href="{{ url('profiles/'. $thread->creator->name) }}">
                    {{ $thread->creator->name }}
                </a> posted: <span v-text="title"></span>
            </span>

        </div>

    </div>

    <div class="card-body">
        <div class="body" v-html="body"></div>
    </div>

    <div class="card-footer" v-if="authorize('owns', thread)">
        <button class="btn btn-secondary btn-sm" @click="editing = true">Edit</button>
    </div>
</div>
