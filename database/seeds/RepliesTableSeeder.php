<?php

use Illuminate\Database\Seeder;

class RepliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factory->define(\App\Reply::class, function ($faker){
            return [
                'thread_id' => function(){
                    return factory(\App\Thread::class)->create()->id;
                },
                'user_id' => function(){
                    return factory(\App\User::class)->create()->id;
                },
                'body' => $faker->paragraph
            ];
        });
    }
}
