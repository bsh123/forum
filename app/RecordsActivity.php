<?php


namespace App;


use Illuminate\Support\Str;

trait RecordsActivity
{
    // method boot for run before methods: boot + trait name
    protected static function bootRecordsActivity()
    {
        if (auth()->guest()) return null;

        /*When a Thread or reply creates, it's acitivity is creating too*/
        /*
         * static::created(function ($thread) {
            $thread->recordActivity('created');
        });
        */
        /*Dynamic way because of deleted or updated events*/
        foreach (static::getActivitiesToRecord() as $event) {
            static::$event(function ($model) use ($event) {
                $model->recordActivity($event);
            });

            // When a thread is deleteing, it's activity must be deleted too
            static::deleting(function ($model) {
                $model->activity()->delete();
            });
        }
    }

    /*For changing created to for example deleted, override this method in it's class*/
    protected static function getActivitiesToRecord()
    {
        return ['created']; // default
    }
    
    public function recordActivity($event)
    {
        $this->activity()->create([
            'user_id' => auth()->id(),
            'type' => $this->getActivityType($event)
        ]);
        /* because of polymorphic is not needed

         * Activity::create([
            'user_id' => auth()->id(),
            'type' => $this->getActivityType($event),
            'subject_id' => $this->id,
            'subject_type' => get_class($this)
        ]);*/
    }

    public function activity()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    protected function getActivityType($event)
    {
        $type = Str::lower((new \ReflectionClass($this))->getShortName());
//        return $event . '_' . $type;
        return "{$event}_{$type}";
    }
}