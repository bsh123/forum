<?php

namespace App;

trait Favoritable
{

    protected static function bootFavoritable()
    {
        /*When you are deleteing assosiated model, it's favorites table's record must be deleted too*/
        static::deleting(function ($model) {
            /*without () after favorites, give us collection*/
            $model->favorites->each->delete();
        });
    }

    public function isFavorited()
    {
        return !!$this->favorites->where('user_id', auth()->id())->count();
    }

    public function getIsFavoritedAttribute()
    {
        return $this->isFavorited();
    }

    public function getFavoritesCountAttribute()
    {
        return $this->favorites->count();
    }

    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'favorited');
    }


    public function favorite()
    {
        $user_id = ['user_id' => auth()->id()];

        /*
         * because of MorphToMany this method not needed
         *
         * Favorite::create([
            'user_id' => auth()->id(),
            'favorited_id' => $reply->id,
            'favorited_type' => get_class($reply)
        ]);*/
        if (!$this->favorites()->where('user_id', $user_id)->exists()) {
            return $this->favorites()->create($user_id);
        }
    }

    public function unfavorite()
    {
        $user_id = ['user_id' => auth()->id()];

        /*each is for related tables that is activities here*/
        $this->favorites()->where($user_id)->get()->each->delete();
    }
}