<?php

use Illuminate\Database\Seeder;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factory->define(\App\Channel::class, function ($faker){
            $name = $faker->word;
            return [
                'name' => $name,
                'slug' => $name
            ];
        });
    }
}
