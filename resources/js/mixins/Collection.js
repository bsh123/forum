/*this file is like trait in laravel*/
export default {
    data() {
        return {
            items: []
        };
    },

    methods: {
        add(item) {
            this.items.push(item);

            this.$emit('added');
        },

        remove(index) {
            this.items.splice(index, 1);

            this.$emit('removed');
        },

        // decodeHtml(html) {
        //     let txt = document.createElement("textarea");
        //     txt.innerHTML = html;
        //     return txt.value;
        // }
    }
}