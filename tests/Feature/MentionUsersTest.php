<?php

namespace Tests\Feature;

use App\Reply;
use App\Thread;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MentionUsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function mentioned_users_in_a_reply_are_notified()
    {
        // Given I have a user, JohnDoe, who is signed in.
        $john = create(User::class, ['name' => 'JohnDoe']);
        $this->signIn($john);

        // And another user, JaneDoe
        $jane = create(User::class, ['name' => 'JaneDoe']);

        // If we have a thread
        $thread = create(Thread::class);

        // And JohnDoe replies and mentions @JaneDoe.
        $reply = make(Reply::class, [
            'body' => '@JaneDoe look at this.'
        ]);
        $this->json('post', $thread->path() . '/replies', $reply->toArray());

        // Then JaneDoe should be notified.
        $this->assertCount(1, $jane->notifications);
    }

    /** @test */
    public function it_can_fetch_all_mentioned_users_starting_with_the_given_characters()
    {
        create(User::class,['name' => 'johndoe']);
        create(User::class,['name' => 'johndoe2']);
        create(User::class,['name' => 'janedoe']);

        $results = $this->json('GET', '/api/users', ['name' => 'john']);
        $this->assertCount(2, $results->json());
    }
}
