@forelse($threads as $thread)
    <div class="card form-group">
        <div class="card-header">
            <div class="level">

                <div class="flex">
                    <h4>
                        <a href="{{ $thread->path() }}">
                            @if(auth()->check() && $thread->hasUpdatedFor(auth()->user()))
                                <strong>{{ $thread->title }}</strong>
                            @else
                                {{ $thread->title }}
                            @endif
                        </a>
                    </h4>

                    <h5>Posted by: <a href="{{ route('profile', $thread->creator) }}">{{ $thread->creator->name }}</a></h5>
                </div>

                <a href="{{ $thread->path() }}">{{ $thread->replies_count }}
                    {{ \Illuminate\Support\Str::plural('comment', $thread->replies_count) }}
                </a>
            </div>

        </div>

        <div class="card-body">
            <div class="body">{!! $thread->body !!}</div>
        </div>
        <div class="card-footer">
            {{ $thread->visits()->count() }} <i class="fa fa-eye"></i>
        </div>
    </div>
@empty
    <h3>There are no records</h3>
@endforelse