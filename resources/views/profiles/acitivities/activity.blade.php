<div class="card">
    <div class="card-header">
        <div class="level">

            <div class="flex">
                {{ $heading }}
            </div>

        </div>
        <hr>
        <div class="card-body">
            {!! $body !!}
        </div>

    </div>
</div>
