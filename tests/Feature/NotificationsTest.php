<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Notifications\DatabaseNotification;
use Tests\TestCase;

class NotificationsTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $this->signIn();

    }

    /** @test */
    public function a_notification_is_prepared_when_a_subscribed_thread_recives_new_reply_that_is_not_by_the_current_user()
    {
        $thread = create('App\Thread')->subscribe();
        $this->assertCount(0, auth()->user()->notifications);

        // each time a new reply is left...
        $thread->addReply([
            'user_id' => auth()->id(),
            'body' => 'some reply'
        ]);

        // A notification should be prepared for the user
        $this->assertCount(0, auth()->user()->fresh()->notifications);

        // new reply left. new reply for user should notify
        $thread->addReply([
            'user_id' => create('App\User')->id,
            'body' => 'some reply'
        ]);
        $this->assertCount(1, auth()->user()->fresh()->notifications);
    }

    /** @test */
    public function a_user_can_fetch_their_unread_notifications()
    {
        /*short form - comes from database/factories/ModelFactory*/
        create(DatabaseNotification::class);

        /**
         * Long form
        $thread = create('App\Thread')->subscribe();

        $thread->addReply([
            'user_id' => create('App\User')->id,
            'body' => 'some reply'
        ]);*/

        $this->assertCount(
            1,
            $this->getJson("profiles/".auth()->user()->name."/notifications")->json()
        );
    }

    /** @test */
    public function a_user_can_mark_a_notification_as_read()
    {
        create(DatabaseNotification::class);

        $user = auth()->user();
        $this->assertCount(1, $user->unreadNotifications);

        $notificationId = $user->unreadNotifications->first()->id;

        $this->delete("profiles/{$user->name}/notifications/{$notificationId}");
        $this->assertCount(0, $user->fresh()->unreadNotifications);
    }
}
