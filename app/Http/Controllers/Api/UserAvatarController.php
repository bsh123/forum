<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class UserAvatarController extends Controller
{
    public function store()
    {
        request()->validate(['avatar' => 'required | image']);

        $path = request()->file('avatar')->store('avatars', 'public');
        $user = auth()->user();
        $user->avatar_path = $path;
        $user->save();
        return response([],204);
    }
}
