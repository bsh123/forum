@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-8">
                @include('threads._list')
                {{ $threads->links() }}
            </div>

            <div class="col-md-4">
                @if(count($trending))
                    <div class="card form-group">

                        <div class="card-header">
                            Trending Threads
                        </div>
                        <div class="card-body">
                            <ul class="list-group">
                                @foreach($trending as $trend)
                                    <li class="list-group-item"><a
                                                href="{{ url($trend->path) }}">{{str_limit($trend->title, 35)}}</a></li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
