<?php


namespace App\Filters;


use App\User;

class ThreadFilters extends Filters
{
    protected $filters = ['by', 'popular', 'unanswered'];

    /**
     * Filter the query by a given username
     * @param string $username
     * @return mixed
     */
    protected function by($username)
    {
        $user = User::where('name', $username)->firstOrFail();

        return $this->builder->where('user_id', $user->id);
    }

    /**
     * Filter the query by most popular threads by replies.
     * @return $this
     */
    protected function popular()
    {
        /*for disabling latest() in $this->getThreads() in @index of ThreadsController*/
        $this->builder->getQuery()->orders =[];

        return $this->builder->orderBy('replies_count', 'desc');
    }

    protected function unanswered()
    {
        return $this->builder->where('replies_count', 0);
    }

}