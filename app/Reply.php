<?php

namespace App;

use App\Notifications\YouWereMentioned;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Stevebauman\Purify\Facades\Purify;

class Reply extends Model
{
    /*RecordsActivity trait is for registering activity about CRUD of Reply*/
    /*Favoritable trait is for reply can be favorited*/
    use Favoritable, RecordsActivity;

    protected $fillable = ['thread_id', 'user_id', 'body'];

    /* for global scope Eager Loading owner and favorites*/
    protected $with = ['owner', 'favorites'];

    /* when we use Json, we appends attributes that we want use it
    * we need this property for Favorite.vue in data() => favoritesCount: this.reply.favoritesCount
    * favoritesCount comes from getfavoritesCountAttribute
    */
    protected $appends = ['favoritesCount', 'isFavorited'];

    protected static function boot()
    {
        parent::boot();

        // for increment replies_count in threads table, when a reply added
        static::created(function ($reply) {
            $reply->thread->increment('replies_count');
        });
        static::deleted(function ($reply) {
            $reply->thread->decrement('replies_count');
        });
    }


    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    public function wasJustPublished()
    {
        return $this->created_at->gt(Carbon::now()->subMinute());
    }

    public function path()
    {
        return $this->thread->path() . "#reply-{$this->id}";
    }

    public function mentionedUsers()
    {
        // Inspect the body of the reply for username mentions
        /* preg_match_all is for all usernames that mentioned */
        preg_match_all('/\@([\w\-]+)/', $this->body, $matches);
        return $matches[1];
    }

    public function setBodyAttribute($body)
    {
        $this->attributes['body'] = preg_replace('/\@([\w\-]+)/', "<a href='/profiles/$1'>$0</a>", $body);
    }

    public function getBodyAttribute($body)
    {
        return Purify::clean($body);
    }

}
