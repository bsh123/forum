@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <avatar-form :user="{{ $profileUser }}"></avatar-form>

                @forelse($activities as $date => $activity)
                    <h3 class="mt-3 pb-2 border-bottom">{{ $date }}</h3>

                    @foreach($activity as $record)
                        <div class="form-group">
                            @if(view()->exists("profiles.acitivities.{$record->type}"))
                                @include("profiles.acitivities.{$record->type}", ['activity' => $record])
                            @endif
                        </div>
                    @endforeach
                @empty
                    <p>There is no activity for this user yet.</p>
                @endforelse

            </div>
        </div>
    </div>

@endsection