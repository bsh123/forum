<?php

namespace Tests\Feature;

use App\Activity;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Thread;
use App\Reply;

class ActivityTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_records_activity_when_a_thread_created()
    {
        $this->signIn();
        $thread = create(Thread::class);

        $this->assertDatabaseHas('activities', [
            'type' => 'created_thread',
            'user_id' => auth()->id(),
            'subject_id' => $thread->id,
            'subject_type' => Thread::class
        ]);

        /*morphTo way*/
        $activity = Activity::first();
        $this->assertEquals($activity->subject->id, $thread->id);
    }

    /** @test */
    public function it_records_activity_when_a_reply_created()
    {
        $this->signIn();
        $reply = create(Reply::class);

        /*we expect 2 records because when we create a reply here, in model factory thread is creating too
        and should register 2 records for both of theme in activities table*/
        $this->assertEquals(2, Activity::count());
    }

    /** @test */
    public function it_fetches_feed_for_any_user()
    {
        // this test is for group threads and replies in user's profile by Date

        // Given we have Threads
        $this->signIn();
        create(Thread::class, ['user_id' => auth()->id()], 2);
        auth()->user()->activity()->take(1)->update(['created_at' => Carbon::now()->subWeek()]);

        // when we fetch their feed
        $feed = Activity::feed(auth()->user(), 50);

        // Then, it should be returned in proper format
        $this->assertTrue($feed->keys()->contains(
            Carbon::now()->format('Y-m-d')
        ));
        $this->assertTrue($feed->keys()->contains(
            Carbon::now()->subWeek()->format('Y-m-d')
        ));
    }
}
