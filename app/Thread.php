<?php

namespace App;

use App\Events\ThreadReceivedNewReply;
use Illuminate\Database\Eloquent\Model;
use Stevebauman\Purify\Facades\Purify;


class Thread extends Model
{
    use RecordsActivity;

    protected $guarded = [];
    protected $with = ['creator', 'channel'];

    // For adding getIsSubscribedToAttributes to output array
    protected $appends = ['isSubscribedTo'];

    /*******eager load example*******/
    /**
     * protected static function boot()
     * {
     * parent::boot();
     *
     * static::addGlobalScope('replyCount', function ($builder){
     * $builder->withCount('replies');
     * });
     * }*/

    public function path()
    {
        return "/threads/{$this->channel->slug}/{$this->id}";
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    /**
     * @param array $reply
     * @return Model
     */
    public function addReply($reply)
    {
        $reply = $this->replies()->create($reply);

        /*Send Notifications for subscriptions*/
        /*Send Notifications for username who mentioned in reply - when RepliesController@store is called*/

        event(new ThreadReceivedNewReply($reply));


        return $reply;
    }

    // For filtering
    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    /**
     * @param int null $userId
     * @return $this
     */
    public function subscribe($userId = null)
    {
        $this->subscriptions()->create([
            'user_id' => $userId ?: auth()->id()
        ]);

        return $this;
    }

    public function unsubscribe($userId = null)
    {
        $this->subscriptions()
            ->where('user_id', $userId ?: auth()->id())
            ->delete();
    }

    public function subscriptions()
    {
        return $this->hasMany(ThreadSubscription::class);
    }

    public function getIsSubscribedToAttribute()
    {
        return $this->subscriptions()
            ->where('user_id', auth()->id())
            ->exists();
    }

    public function hasUpdatedFor($user = null)
    {
        $user = $user ?: auth()->user();
        // Look in the cache for the proper key.
        // Compare that carbon instance with the $thread->updated_at
        $key = $user->visitedThreadCacheKey($this);

        // returns $this->updated_at if is > than $key = it means unread
        return $this->updated_at > cache($key);
    }

    public function visits()
    {
        return new Visits($this);
    }

    public function getBodyAttribute($body)
    {
        return Purify::clean($body);
    }
}
