<?php

use Illuminate\Database\Seeder;

class ThreadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factory->define(\App\Thread::class, function ($faker){
            return [
                'user_id' => function(){
                    return factory(\App\User::class)->create()->id;
                },
                'channel_id' => function(){
                    return factory(\App\Channel::class)->create()->id;
                },
                'title' => $faker->sentence,
                'body' => $faker->paragraph
            ];
        });
    }
}
