require('./bootstrap');

window.events = new Vue();

// flash ('my new flash message')
window.flash = function(message, level = 'success') {
    window.events.$emit('flash', {message, level});
};

let authorizations = require('./authorizations');

Vue.prototype.authorize = function (...params) {
    // Here you can add admin access to all privileges
    // if (window.App.user.type == 1){// return true;}

    // return  true;  /*for testing*/

    if (! window.App.signedIn) return false;

    if (typeof params[0] === 'string') {
        return authorizations[params[0]](params[1]);
    }

    return params[0](window.App.user);
};

Vue.prototype.signedIn = window.App.signedIn;

Vue.component('flash', require('./components/Flash.vue'));
Vue.component('thread-view', require('./pages/Thread.vue'));
Vue.component('paginator', require('./components/Paginator.vue'));
Vue.component('user-notifications', require('./components/UserNotifications.vue'));
Vue.component('avatar-form', require('./components/AvatarForm.vue'));
Vue.component('wysiwyg', require('./components/Wysiwyg.vue'));

const app = new Vue({
    el: '#app'
});
