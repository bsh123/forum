@extends('layouts.app')
@section('styles')
    <link href="{{ asset('css/jquery.atwho.css') }}" rel="stylesheet">
@endsection

@section('content')
    <thread-view :thread="{{ $thread }}" inline-template>
        <div class="container" v-cloak>
            <div class="row">
                <div class="col-md-8">

                    @include('threads._thread_card')

                    <replies @added="repliesCount++"  {{--this.$emit('added');--}}
                             @removed="repliesCount--">
                    </replies>

                    <br>
                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <p>
                                This thread was published {{ $thread->created_at->diffForHumans() }} by
                                <a href="#">{{ $thread->creator->name }}</a>, and currently has
                                <span v-text="repliesCount"></span>
                                {{ \Illuminate\Support\Str::plural('comment', $thread->replies_count) }}.
                            </p>

                            <p>
                                {{--json_encode is for bool output--}}
                                <subscribe-button :active="{{ json_encode($thread->isSubscribedTo) }}"></subscribe-button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </thread-view>
@endsection
